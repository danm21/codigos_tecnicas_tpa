#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include "operaciones.hpp"
#include <cmath>

// Un array con cálculos previamente realizados, usados en las funciones fscanModelo, zscanModelo y pscanModelo
static const vector<double> ter {1.0, 0.666667, 0.533333, 0.457143, 0.406349, 0.369408, 0.340992, 0.31826, 0.299538, 0.283773, 0.27026, 0.25851};

// Modela la técnica F-scan
vector<double> fscanModelo(const vector<double>& focal, double beta, const double param[11]){
    // Cada casilla de param lleva los valores de los parámetros siguiendo este orden
    // L  Pavg    Tp  wl  D   ds  Cf  R   alpha freq  l_foco
    // 0  1       2   3   4   5   6   7   8     9     10

    int focal_size = (int) focal.size();
    double Leff = (1. - exp(-param[8] * param[0]))/param[8];
    vector<double> w0(focal_size);
    vector<double> z0(focal_size);
    vector<double> w(focal_size);
    vector<double> I0(focal_size);
    vector<double> B(focal_size);

    for(int j=0;j<focal_size;j++){
        w0[j] = 2.0*param[3]*focal[j]*param[6]/(M_PI*param[4]); // Radio de la cintura del haz
        z0[j] = M_PI*pow(w0[j],2)/param[3];  // Rango de Rayleigh
        w[j]  = w0[j]*sqrt(1.0 + pow((param[5] - focal[j])/z0[j],2.0)); // Radio del haz
        I0[j] = 2.0*log(1.0 + sqrt(2.0))*param[1]/(param[2]*param[9]*M_PI*pow(w[j],2.0)); // Intensidad pico sobre la muestra
        B[j]  = beta*(1.0 - param[7])*I0[j]*Leff;
    }

    // Número de términos usados en la productoria
    int N = 11;

    vector<double> T(focal_size);
    double val;

    for (int cont=0; cont < focal_size; cont++){
        val = 0.0;
        for(int m=0; m < N + 1; m++){
            val = val + pow(-B[cont],m)/(m + 1.0)*ter[m];
        }
        T[cont]=val;
    }
    return T;
}

// Modela la técnica Z-scan
vector<double> zscanModelo(const vector<double>& zpos, double beta, const double param[11]){
    // Cada casilla de param lleva los valores de los parámetros siguiendo este orden, ds no es usado en ningún momento
    // L  Pavg    Tp  wl  D   ds  Cf  R   alpha freq  l_foco
    // 0  1       2   3   4   5   6   7   8     9     10

    int zpos_size = (int) zpos.size();
    double Leff = (1. - exp(-param[8] * param[0]))/param[8];
    double w0;
    double z0;
    vector<double> w(zpos_size);
    vector<double> I0(zpos_size);
    vector<double> B(zpos_size);
    
    w0 = 2.0*param[3]*param[10]*param[6]/(M_PI*param[4]); // Radio de la cintura del haz
    z0 = M_PI*pow(w0,2)/param[3];                         // Rango de Rayleigh

    for(int j=0;j<zpos_size;j++){
        w[j]  = w0*sqrt(1.0 + pow((param[10] - zpos[j])/z0,2.0)); // Radio del haz
        I0[j] = 2.0*log(1.0 + sqrt(2.0))*param[1]/(param[2]*param[9]*M_PI*pow(w[j],2.0)); // Intensidad pico sobre la muestra
        B[j]  = beta*(1.0 - param[7])*I0[j]*Leff;
    }

    // Número de términos usados en la productoria
    int N = 11;

    vector<double> T(zpos_size);
    double val;

    for (int cont=0; cont < zpos_size; cont++){
        val = 0.0;
        for(int m=0; m < N + 1; m++){
            val = val + pow(-B[cont],m)/(m + 1.0)*ter[m];
        }
        T[cont]=val;
    }
    return T;
}

// Modela la técnica P-scan
vector<double> pscanModelo(const vector<double>& potencias, double beta, const double param[11]){
    // Cada casilla de param lleva los valores de los parámetros siguiendo este orden
    // L  Pavg    Tp  wl  D inty    Cf  R   alpha freq  l_foco
    // 0  1       2   3   4 5       6   7   8     9     10

    // Se renombra el parámetro "ds" como "inty" intercepto con el eje y, se toma 16.86 cm como la posición de la muestra
    vector<double> Tplot(potencias.size());
    for (long unsigned int i = 0; i < potencias.size(); i++){
        double parametros[11] = {param[0], potencias[i], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], 0.1686};
        Tplot[i] = zscanModelo({param[10]}, beta, parametros)[0]-abs(param[5]);
    }

    return Tplot;
}


// Encuentra el valor máximo en el array
double valorMaximo(const vector<double>& vec){
    double valor=vec[0];
    for(size_t i=1;i<vec.size();i++){
        if (vec[i]>valor){
            valor = vec[i];
        }
    }
    return valor;
}

// Encuentra el valor y posición en el array donde está el valor mínimo
void posicionValorMin(const vector<double>& vec, double &pos, double &valor){
    valor = vec[0];
    pos = 0.0;
    for(size_t i=1;i<vec.size();i++){
        if (vec[i]<valor){
            valor = vec[i];
            pos   = (double) i;
        }
    }
}

// Calcula un valor que sirve para evaluar que tan cercano es el ajuste a los datos experimentales
double metrica(funref function, double beta, const vector<double>& focal, const vector<double>& datos, const double param[11]){
    double mdatos, pmdatos, mT, pmT;

    vector<double> T(focal.size());

    T = function(focal, beta, param);
    posicionValorMin(datos, mdatos, pmdatos);
    posicionValorMin(T, mT, pmT);
    /* Se calcula la métrica basandose en la diferencia entre el modelo y los datos experimentales, también se incluye
       la diferencia entre el punto mínimo de los datos experimentales y el modelo.
      */
    return 5*sqrt(pow(mdatos-mT,2) + pow(pmdatos - pmT,2)) + norma2(datos, T);
}

// Optimiza la función "function", encontrando su mínimo, en este caso beta
double nelderMead(funref function, double alpha, double gamma, double tol, const vector<double>& focal, const vector<double>& datos, const double param[11]){
    // Usar siempre un gamma mayor a alpha, se recomienda: alpha=1 gamma=2
    double v1 = 3.0e-10;
    double v2 = 5.0e-9;
    double bw[2]={v1,v2};
    double fun[2];
    double mid,xr,xe;
    double fxr,fxe,fmid;
    double t;
    fun[0]=metrica(function, bw[0], focal, datos, param);
    fun[1]=metrica(function, bw[1], focal, datos, param);
    unsigned int iteracion = 0;
    while (abs(bw[0]-bw[1]) > tol && iteracion < 800 ){
        if (fun[0] > fun[1]){
            t = bw[0];
            bw[0]=bw[1];
            bw[1]=t;
            t = fun[0];
            fun[0]=fun[1];
            fun[1]=t;
        } // funciones y puntos organizados
        mid=(bw[0]+bw[1])/2.0;

        xr =  bw[0] + alpha*(mid - bw[1]);
        fxr = metrica(function, xr, focal, datos, param);

        if (fxr < fun[0]){
            xe = bw[0] + gamma * (mid - bw[1]);
            bw[1] = xr;
            fun[1] = fxr;
            fxe = metrica(function, xe, focal, datos, param);
            if (fxe < fxr){
                bw[1] = xe;
                fun[1] = fxe;
            }
        }
        else {
            fmid=metrica(function, mid, focal, datos, param);
            if (fxr < fun[1]){
                bw[1] = xr;
                fun[1] = fxr;}
            if (fmid < fun[1]){
                bw[1] = mid;
                fun[1] = fmid;}
        }
        iteracion++;
    }
    return bw[0];
}

// Calcula el error cuadrático medio entre dos vectores
double ECM(const vector<double>& v1, const vector<double>& v2){
    double sum = 0;
    for(unsigned int i=0; i<v1.size(); i++){
        sum += pow(v1[i]-v2[i], 2)/v2[i];
    }
    return sum/(static_cast<float>(v1.size()));
}

double mediaPonderada(const vector<double>& v, const vector<double>& weights){
    double sum = 0;
    double norma = 0;
    for(unsigned int i=0; i<v.size(); i++){
        sum   += weights[i]*v[i];
        norma += weights[i];
    }
    return sum/norma;
}

double media(const vector<double>& v){
    double sum = 0;
    for(unsigned int i=0; i<v.size(); i++){
        sum += v[i];
    }
    return sum/v.size();
}

// Calcula la normal al cuadrado de la diferencia de dos vectores
double norma2 (const vector<double>& v1, const vector<double>& v2) {
    double sum = 0;
    for(unsigned int i=0; i<v1.size(); i++){
        sum += pow(v1[i]-v2[i], 2);
    }
    return sum;
}
