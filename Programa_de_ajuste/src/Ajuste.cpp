#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <random>
#include <chrono>
#include <cstring>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include <omp.h>

#include "operaciones.hpp"

using namespace std;

namespace py = pybind11;

vector<vector<double>> ajustar(char option[4], vector<double> focal, vector<double> datos,vector<double> input, vector<double> error, double tol, double TH, size_t NN){
/*   Cada casilla de param lleva los valores de los parámetros siguiendo este orden
     L  Pavg    Tp  wl  D   ds  Cf  R   alpha freq  l_foco
     0  1       2   3   4   5   6   7   8     9     10

    El array "error" aloja el conjunto de errores de los parámetros como sigue:
      L_e Pavg_e  Tp_e  wl_e  D_e  ds_e  Cf_e R_e   alphal_e    freq_e  lfoco_e
      0   1       2     3     4    5     6    7     8           9       10
*/
    // Comprueba las dimensiones del vector enviado desde Python
    if(input.size()<11 || error.size()<11){
        cout<<"Hacen falta parámetros"<<endl;
    }

    // Inicio de conteo de tiempo transcurrido ,con el fin de usarlo como semilla para la generación de valores aleatorios
    auto start = std::chrono::steady_clock::now();

    vector<double> ECMs(NN);
    vector<double> Betas(NN);
    vector<vector<double>> Best;

    // Inicialización de distribuciones normales con sus correspondientes valores medios y desviaciones estándar
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = end-start;
    default_random_engine generator(diff.count()*1e9);
    normal_distribution<double> L_dist(input[0], error[0]);
    normal_distribution<double> Pavg_dist(input[1], error[1]);
    normal_distribution<double> Tp_dist(input[2], error[2]);
    normal_distribution<double> wl_dist(input[3], error[3]);
    normal_distribution<double> D_dist(input[4], error[4]);
    normal_distribution<double> ds_dist(input[5], error[5]);
    normal_distribution<double> Cf_dist(input[6], error[6]);
    normal_distribution<double> R_dist(input[7], error[7]);
    normal_distribution<double> alpha_dist(input[8], error[8]);
    normal_distribution<double> freq_dist(input[9], error[9]);
    normal_distribution<double> l_foco_dist(input[10], error[10]);

    double beta_final=0;


    // Cambia la función a ajustar en base a la entrada del usuario: option
    // En caso de no introducir ninguna, se elige f-scan
    funref funcionModelo = NULL;
    if (std::strcmp(option,"fscan")==0) {
        funcionModelo = fscanModelo;
        cout<<"Se ajusta f-scan"<<endl;
    }
    else if (std::strcmp(option,"zscan")==0) {
        funcionModelo = zscanModelo;
        cout<<"Se ajusta z-scan"<<endl;
    }
    else if (std::strcmp(option,"pscan")==0) {
        funcionModelo = pscanModelo;
        cout<<"Se ajusta p-scan"<<endl;
    }
    else {
        funcionModelo = fscanModelo;
        cout<<"Función escogida erronea, se toma f-scan"<<endl;
    }

    //omp_set_num_threads(4);

    // Cada iteración se ejecuta en un hilo separado
    #pragma omp parallel  for reduction (+:beta_final)
    for (size_t i = 0; i < NN; i++)
    {
      // Sección en la que solo un hilo puede acceder simultáneamente, esto es necesario para eliminar posibles condiciones de carrera,
      // debido a que el generador es común y tiene estados internos que cambian cada vez que se llama*/
      double L, Pavg, Tp, wl, D, ds, Cf, R, alpha, freq, l_foco;
      #pragma omp critical
      {
      L = L_dist(generator);        // Espesor de la muestra [m].
      Pavg = Pavg_dist(generator);  // Potencia promedio [W]
      Tp = Tp_dist(generator);      // Ancho de pulso FWHM [s]
      wl = wl_dist(generator);      // Longitud de onda [m]
      D = D_dist(generator);        // Diametro del haz [m]
      ds = ds_dist(generator);      // Distancia entre la muestra y la EFTL [m] (usado para F-scan)
      Cf = Cf_dist(generator);      // Factor de corrección EFTL
      R = R_dist(generator);        // Coeficiente de reflección
      alpha = alpha_dist(generator);// Absorción lineal
      freq = freq_dist(generator);    // Tasa de repetición del láser
      l_foco = l_foco_dist(generator);  // Distancia entre el foco del haz y la EFTL [m] (usado para Z-scan)
      }
      double parametros[11]={L, Pavg, abs(Tp), wl, D, ds, Cf, R, alpha, freq, l_foco};

      //Búsqueda de mínimo, se recomienda usar alpha=0.5 y gamma =2.0, ya que ofrece una convergencia más rápida.
      Betas[i] = nelderMead(funcionModelo, 0.5, 2.0, tol, focal, datos, parametros);
      beta_final += Betas[i];

      vector<double> Tplot = funcionModelo(focal, Betas[i], parametros);
      ECMs[i] = ECM(Tplot, datos);

      // Implementación "Best"
      if (ECMs[i]<TH){
        #pragma omp critical // Necesario debido a que Best es un array compartido entre los hilos
        {
        Best.push_back({ECMs[i], Betas[i], L, Pavg, Tp, wl, D, ds, Cf, R, alpha, freq, l_foco});
        }
      }

    }

    // Generación de vector "1.0/ECMs^2"
    vector<double> weights(NN);
    for(size_t j=0; j<NN; j++){
        weights[j]=pow(1.0/ECMs[j],2);
    }

    // RESULTADOS EN BASE A MEDIA PONDERADA
    double BETA = mediaPonderada(Betas, weights);

    vector<double> BETAvec(NN);
    for(size_t j=0; j<NN; j++){
        BETAvec[j]=pow(Betas[j]-BETA,2);
    }
    double error_beta = sqrt(mediaPonderada(BETAvec, weights));

    cout<<"_____________________"<<endl;
    cout<<"Valor de Beta: "<<BETA*1e11<<"    Error: "<<error_beta*1e11<<" cm/GW"<<endl;
    cout<<"ECM promedio: "<<media(ECMs)<<endl;
    cout<<"Iteraciones usadas: "<<NN<<endl;
    cout<<"Promedio G. betas: "<<beta_final/NN<<endl;
    cout<<"_____________________"<<endl;

    return Best;
}


// Exponen las funciones F-scan, Z-scan y P-scan para que sean accesibles en python

vector<double> fscanModelo_Externo(const vector<double>& focal, double beta, vector<double> input){

    double param[11] = {input[0], input[1], input[2], input[3], input[4], input[5], input[6], input[7], input[8], input[9], input[10]};
    return fscanModelo(focal, beta, param);
}

vector<double> zscanModelo_Externo(const vector<double>& zpos, double beta, vector<double> input){

    double param[11] = {input[0], input[1], input[2], input[3], input[4], input[5], input[6], input[7], input[8], input[9], input[10]};
    return zscanModelo(zpos, beta, param);
}

vector<double> pscanModelo_Externo(const vector<double>& potencias, double beta, vector<double> input){

    double param[11] = {input[0], input[1], input[2], input[3], input[4], input[5], input[6], input[7], input[8], input[9], input[10]};
    return pscanModelo(potencias, beta, param);
}

PYBIND11_MODULE(ajustemod, module_handle) {
    module_handle.doc() = "Librería de Ajuste";
    module_handle.def("ajustar", &ajustar);
    module_handle.def("fscan_cpp", &fscanModelo_Externo);
    module_handle.def("zscan_cpp", &zscanModelo_Externo);
    module_handle.def("pscan_cpp", &pscanModelo_Externo);
}
