# Código de Ajuste F-scan, Z-scan y P-scan

## Funciones disponibles

### Función "ajustar", Uso en Python

Para realizar ajuste desde un script Python o Jupyter notebook , a la función **ajustar** se le debe introducir los parámetros usando el siguiente orden:
El array "parámetros" aloja el conjunto de parámetros como sigue:

| Parámetro    | L   | Pavg | Tp  | wl  | D   | ds  | Cf  | R   | alpha | freq | l_foco |
| ------------ | --- | ---- | --- | --- | --- | --- | --- | --- | ----- | ---- | ------ |
| Componente # | 0   | 1    | 2   | 3   | 4   | 5   | 6   | 7   | 8     | 9    | 10     |

El array "error" aloja el conjunto de errores de los parámetros como sigue: 

| L_e | Pavg_e | Tp_e | wl_e | D_e | ds_e | Cf_e | R   | alpha | freq | l_foco_e |
| --- | ------ | ---- | ---- | --- | ---- | ---- | --- | ----- | ---- | -------- |
| 0   | 1      | 2    | 3    | 4   | 5    | 6    | 7   | 8     | 9    | 7        |

- **ds** no es usado en Z-scan

- **L_foco** no es usado en F-scan

**En el caso de P-scan**

| L   | Nousado | Tp  | wl  | D   | Inty | Cf  | R   | alpha | freq | l_foco |
| --- | ------- | --- | --- | --- | ---- | --- | --- | ----- | ---- | ------ |
| 0   | 1       | 2   | 3   | 4   | 5    | 6   | 7   | 8     | 9    | 10     |

| L_e | Pavg_e | Tp_e | wl_e | D_e | Inty_e | Cf_e | R   | alpha | freq | l_foco_e |
| --- | ------ | ---- | ---- | --- | ------ | ---- | --- | ----- | ---- | -------- |
| 0   | 1      | 2    | 3    | 4   | 5      | 6    | 7   | 8     | 9    | 7        |

- **Inty** es un número positivo que dicta el intercepto de la gráfica de transmitancia para P-scan con el eje y, desplazando la gráfica abajo de 1.

- **Pavg** no es usado en este caso

**Uso en archivo Python o Jupyter Notebook**

```python
# "build" es la carpeta en donde se encuentra la librería
import build.ajustes as am
am.ajustar(Nombre Técnica, Eje x, Transmitancia, Vector de parámetros,
           Vector de Errores, Tolerancia, Threshold, N. Muestras)
```

Los argumentos en orden de esta función son:

- **Nombre Técnica:** Se debe elegir "fscan" ó "zscan" según sea el caso.
- **Eje X:** Se introduce un array que tiene las posiciones del desplazamiento que se hizo en la técnica, ej: fscan, posiciones focales; p-scan, potencias.
- **Transmitancia:** Se introduce un array las transmitancias medida **normalizada**
- **V. parámetros:** Se introduce el array que contiene los parámetros del montaje experimental siguiendo el formato mostrado anteriormente.
- **V. Errores:** Se introduce el array que contiene los errores de los parámetros del montaje experimental siguiendo el formato mostrado anteriormente.
- **Tolerancia:**  Diferencia máxima entre valores internos alcanzados por el algoritmo de búsqueda "Nelder-Mead".
- **Threshold:** Umbral máximo de ECM.
- **# de muestras:** Cantidad de muestras  que deberá realizar y evaluar para llegar al ajuste.

### Función "fscan_cpp" y "zscan_cpp", Uso en Python

| L focales Array | Beta | Vector |      |     |     |     |     |     |     |       |      |        |
| --------------- | ---- | ------ | ---- | --- | --- | --- | --- | --- | --- | ----- | ---- | ------ |
|                 |      | L      | Pavg | Tp  | wl  | D   | ds  | Cf  | R   | alpha | freq | l_foco |
| 0               | 1    | 2      | 3    | 4   | 5   | 6   | 7   | 8   | 9   | 10    | 11   | 12     |

## Función "pscan_cpp", Uso en Python

| Potencias Array | Beta | Vector |         |     |     |     |      |     |     |       |      |        |
| --------------- | ---- | ------ | ------- | --- | --- | --- | ---- | --- | --- | ----- | ---- | ------ |
|                 |      | L      | NoUsado | Tp  | wl  | D   | Inty | Cf  | R   | alpha | freq | l_foco |
| 0               | 1    | 2      | 3       | 4   | 5   | 6   | 7    | 8   | 9   | 10    | 11   | 12     |

## Compilación y prerequisitos

## Windows

### Prerequisitos

- Instalar Git
- Instalar Cmake y seleccionar la opción añadir PATH
- Instalar Visual Studio community 2019

### Compilación

- Entrar a la carpeta build
- Darle click derecho a la carpeta y abrir Git BASH
- Escribir "cmake .." en esta terminal
- Abrir el archivo "Ajustes.sln" en Visual Studio
- Darle en la pestaña que dice "Debug" y cambiarla por "Release"
- Darle click derecho en "Ajustedmod" y darle compilar
- En la carpeta "Build" se generará una carpeta llamada "Release", la cual tiene la compilación del código.
- El ejecutable resultante que se requerirá posteriormente es "ajustemod.cp37-win_amd64.pyd"

## Distribuciones Linux

### Prerequisitos

- Instalar Cmake, make.
- Instalar herramientas desarrollador python, `sudo apt-get install python3-dev` en el caso de Ubuntu.

### Compilación

- Ubicarse en el directorio **build**
- Escribir `cmake ..`
- Escribir `make`

Lo anterior generará unos archivos ejecutables, los cuales son el ejecutable de ajuste en c++ llamado "ajuste" y su librería "operaciones"

**Posible error y solución:** Si el módulo no es encontrado por python, puede deberse a que el código se compiló para ser ejecutado en una versión de python distinta a la del interprete que lo intenta usar (el comando make muestra para que versión de python fue compilado).
