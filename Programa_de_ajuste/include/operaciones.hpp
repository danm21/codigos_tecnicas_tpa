
#include <vector>
using namespace std;

// Se define un tipo de datos, para las funciones F-scan y Z-scan.
typedef vector<double> (*funref)(const vector<double>&, double, const double*);

vector<double> fscanModelo(const vector<double>& focal, double beta, const double param[11]);

vector<double> zscanModelo(const vector<double>& zpos, double beta, const double param[11]);

vector<double> pscanModelo(const vector<double>& potencias, double beta, const double param[11]);

double valorMaximo(const vector<double>& vec);

void posicionValorMin(const vector<double>& vec, double &pos, double &valor);

double metrica(funref, double beta, const vector<double>& focal, const vector<double>& datos, const double param[11]);

double nelderMead(funref, double alpha, double gamma, double tol, const vector<double>& focal, const vector<double>& datos, const double param[11]);

double ECM(const vector<double>& v1, const vector<double>& v2);

double mediaPonderada(const vector<double>& v, const vector<double>& weights);

double media(const vector<double>& v);

double norma2 (const vector<double>&, const vector<double>&);
