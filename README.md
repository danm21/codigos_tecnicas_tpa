# Códigos para la obtención y ajuste de datos en la medición del coeficiente TPA

- "Codigos_laboratorio" : Ofrece todos los códigos usados para la adquisición de datos.
- "Programa_de_ajuste"  : Ofrece todos los códigos usados el ajuste de los datos experimentales.
