# Códigos Laboratorio

Esta carpeta ofrece la mayoría de los códigos usados en la adquisición de datos y manipulación de elementos del montaje experimental durante la ejecución de las técnicas de medición del coeficiente TPA, entre ellos:

- desplazadorL.py : Controlador del desplazador. Modificación con base en el código realizado por Dinntec SAS.

- opto.py : Controlador de la lente electro-óptica (EFLT). Autor: Steven A. Cholewiak, [Github - Steven A. Cholewiak](https://github.com/OrganicIrradiation/opto).

- Configurador.py: Programa con interfáz gráfica para configurar el estado de diversos elementos en el montaje experimental como: Lente EFTL, desplazador, filtro neutro variable, fase de lock-in Amplifier.

- F-scan.py, Z-scan.py, P-scan.py: Códigos para realizar F-scan, Z-scan y P-scan respectivamente.

> Verificar los puertos COM usados por los dispositivos en todos los códigos, ya que estos son sensibles a cambios de computador o del orden de conexión en los puertos USB.

Por otro lado el directorio "Filtro_arduino" expone el firmware usado en el Arduino para el control del filtro neutro variable. Este código ya conoce como realizar la calibración del disco, moverse una cantidad de pasos o a una cantidad de ellos desde el cero conocido gracias a la calibración.
